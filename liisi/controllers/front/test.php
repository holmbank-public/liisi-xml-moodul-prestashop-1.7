<?php

include(dirname(__FILE__).'/../../lib/httpful.phar');

class LiisiTestModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function init()
	{
		$message = '';
		try
		{
        $response = \Httpful\Request::post(Liisi::$liisi_servers[$this->module->config['LIISI_SERVER']]['url'])
            // ->clientSideCert(dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_CLIENT_CERT'], dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'])
            ->clientSideCert(dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'], dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'])
            ->basicAuth($this->module->config['LIISI_USERNAME'], $this->module->config['LIISI_PASSWORD'])
            ->addOnCurlOption(CURLOPT_SSLVERSION, 1)
            ->body('')
            ->mime(\Httpful\Mime::XML)
            ->send();
		}
		catch (Exception $e)
		{
			$message .= $e->getMessage();
		}

		if ($response->code == 200 && $response->content_type == "application/xml")
		{
			echo json_encode(array('result' => 'success', 'message' => $message));
		}
		else
		{
			echo json_encode(array('result' => 'fail', 'message' => $message));
		}
		exit();
	}

}
