<?php

class LiisiReplyModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function postProcess()
    {
        $data_from_post = trim(file_get_contents('php://input'));
        if (!empty($data_from_post))
        {
            try
            {
                $xml = new SimpleXMLElement($data_from_post);
            }
            catch (Exception $e)
            {
                throw new PrestaShopException('Something went wrong when parsing XML');
            }

            $presta_cart_id = current(explode('-', $xml->OrderId));
            $id_order = Order::getOrderByCartId($presta_cart_id);
            $order = new Order($id_order);

            if ($xml->Status == 'SATISFIED' && $xml->Comment == 'Positive')
            {
                $order->setCurrentState(Configuration::get('LIISI_CONTRACT'));
                Mail::Send(
                    $this->context->language->id,
                    'liisi_contract',
                    $this->module->l('Liisi contract confirmed', 'reply'),
                    array(
                        '{downpayment}' => (string)$xml->Terms->DownPayment
                    ),
                    $xml->Client->Email,
                    $xml->Client->FirstName . ' ' . $xml->Client->Surname,
                    null,
                    null,
                    null,
                    null,
                    $this->module->local_path . 'mails/'
                );
            }
            else if ($xml->Status == 'REJECTED')
            {
                $order->setCurrentState(Configuration::get('LIISI_DENIED'));
                Mail::Send(
                    $this->context->language->id,
                    'liisi_denied',
                    $this->module->l('Liisi application denied', 'reply'),
                    array(),
                    $xml->Client->Email,
                    $xml->Client->FirstName . ' ' . $xml->Client->Surname,
                    null,
                    null,
                    null,
                    null,
                    $this->module->local_path . 'mails/'
                );
            }
        }
    }
}
