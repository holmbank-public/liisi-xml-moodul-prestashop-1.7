    <?php

class LiisiPaymentModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    private $order;

    public function initContent()
    {
        parent::initContent();

        $this->assignFormFields();
        $this->setTemplate('payment_execution.tpl');
    }

    public function setMedia()
    {
        parent::setMedia();

        $this->addCSS($this->module->_path . 'liisi_form.css');
        $this->addJS($this->module->_path . 'liisi_form.js');
    }

    public function postProcess()
    {
        if (Tools::isSubmit('liisi_submit_request'))
        {
            $liisi_fields = array(
                'liisi_firstname' => array(
                    'label' => $this->module->l('Firstname', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_surname' => array(
                    'label' => $this->module->l('Surname', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_email' => array(
                    'label' => $this->module->l('Email', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_idcode' => array(
                    'label' => $this->module->l('Idcode', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_phone' => array(
                    'label' => $this->module->l('Phone', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_county' => array(
                    'label' => $this->module->l('County', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_locality' => array(
                    'label' => $this->module->l('Locality', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_street' => array(
                    'label' => $this->module->l('Street', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_zipcode' => array(
                    'label' => $this->module->l('Zipcode', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_duration' => array(
                    'label' => $this->module->l('Duration', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_downpayment' => array(
                    'label' => $this->module->l('Downpayment', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_first_payment_date' => array(
                    'label' => $this->module->l('First payment date', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_employer' => array(
                    'label' => $this->module->l('Employer', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_timewithemployer' => array(
                    'label' => $this->module->l('Time with employer', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_salary' => array(
                    'label' => $this->module->l('Salary', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_loan_obligations' => array(
                    'label' => $this->module->l('LoanObligations', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_politically_exposed' => array(
                    'label' => $this->module->l('PoliticallyExposed', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
                'liisi_allow_pension_query' => array(
                    'label' => $this->module->l('AllowPensionQuery', 'payment'),
                    'value' => null,
                    'error' => true,
                ),
            );

            $this->getFieldValues($liisi_fields);

            if (!empty($this->errors))
            {
                $this->context->smarty->assign('field_data', $liisi_fields);
            }
            else
            {
                $this->processLiisiRequest($liisi_fields);
            }
        }
        elseif (Tools::isSubmit('liisi_confirm_contract'))
        {
            $this->processLiisiConfirmation(Tools::getValue('liisi_order_id'));
        }
        elseif (Tools::isSubmit('liisi_confirm_pending'))
        {
            $this->processLiisiPending(Tools::getValue('liisi_order_id'));
        }
        else
        {
            $this->assignFormValuesFromAddress();
        }
    }

    private function getFieldValues(&$fields)
    {
        foreach ($fields as $key => &$data)
        {
            $data['value'] = Tools::getValue($key, null);

            $this->validateField($key, $data);
        }
    }

    private function validateField($key, &$field)
    {
        if (empty($field['value']) && ($key !== 'liisi_downpayment') && ($key !== 'liisi_politically_exposed') && ($key !== 'liisi_allow_pension_query'))
        {
            $this->errors[] = sprintf($this->module->l('%s can\'t be empty.', 'payment'), $field['label']);
        }
        else
        {
            $field['error'] = false;
        }

        switch ($key)
        {
            case 'liisi_duration':
				$contract_length = $this->module->getContractDurations();
				$max_length = end($contract_length);
				$min_length = reset($contract_length);

                if ($field['value'] < $min_length || $field['value'] > $max_length)
                {
                    $this->errors[] = sprintf($this->module->l('Duration of payments can\'t be less than %d months and more than %d months.', 'payment'), $min_length, $max_length);
                    $field['error'] = true;
				}
                break;
            case 'liisi_idcode':
                preg_match("/([1-6]{1})(\d{2})(\d{2})(\d{2})\d{3}(\d{1})/", $field['value'], $output_array);

                if(empty($output_array)
                    || ($output_array[3] == '00' || $output_array[3] > 12)
                    || ($output_array[4] == '00' || $output_array[4] > 31)
                )
                {
                    $this->errors[] = $this->module->l('Invalid idcode format.', 'payment');
                    $field['error'] = true;
                }
                break;
            case 'liisi_downpayment':
                $min_downpayment = $this->getMinDownpayment($this->context->cart->getOrderTotal(true, Cart::BOTH));
                if (!is_numeric($field['value']))
                {
                    $this->errors[] = $this->module->l('Downpayment has to be a number.', 'payment');
                    $field['error'] = true;
                }
                elseif ((float)$field['value'] < $min_downpayment)
                {
                    $this->errors[] = sprintf(
                        $this->module->l('Downpayment cannot be less than %.2F.', 'payment'),
                        $min_downpayment
                    );
                    $field['error'] = true;
                }
                break;
            case 'liisi_email':
                if (!filter_var($field['value'], FILTER_VALIDATE_EMAIL))
                {
                    $this->errors[] = $this->module->l('Invalid email format.', 'payment');
                    $field['error'] = true;
                }
                break;
        }

    }

    private function getMinDownpayment($sum)
    {
        return Tools::ps_round(max(
            (float)$this->module->config['LIISI_MIN_DOWNPAYMENT_PERCENTAGE'] * $sum / 100,
            (float)$this->module->config['LIISI_MIN_DOWN_PAYMENT_SUM']
        ), 2);
    }

    private function assignFormFields()
    {
        $county_choices = array(
            array(
                'value' => 'Harju maakond',
                'text' => $this->module->l('Harju maakond', 'payment'),
            ),
            array(
                'value' => 'Hiiu maakond',
                'text' => $this->module->l('Hiiu maakond', 'payment'),
            ),
            array(
                'value' => 'Ida-Viru maakond',
                'text' => $this->module->l('Ida-Viru maakond', 'payment'),
            ),
            array(
                'value' => 'Jõgeva maakond',
                'text' => $this->module->l('Jõgeva maakond', 'payment'),
            ),
            array(
                'value' => 'Järva maakond',
                'text' => $this->module->l('Järva maakond', 'payment'),
            ),
            array(
                'value' => 'Lääne maakond',
                'text' => $this->module->l('Lääne maakond', 'payment'),
            ),
            array(
                'value' => 'Lääne-Viru maakond',
                'text' => $this->module->l('Lääne-Viru maakond', 'payment'),
            ),
            array(
                'value' => 'Põlva maakond',
                'text' => $this->module->l('Põlva maakond', 'payment'),
            ),
            array(
                'value' => 'Pärnu maakond',
                'text' => $this->module->l('Pärnu maakond', 'payment'),
            ),
            array(
                'value' => 'Rapla maakond',
                'text' => $this->module->l('Rapla maakond', 'payment'),
            ),
            array(
                'value' => 'Saare maakond',
                'text' => $this->module->l('Saare maakond', 'payment'),
            ),
            array(
                'value' => 'Tartu maakond',
                'text' => $this->module->l('Tartu maakond', 'payment'),
            ),
            array(
                'value' => 'Valga maakond',
                'text' => $this->module->l('Valga maakond', 'payment'),
            ),
            array(
                'value' => 'Viljandi maakond',
                'text' => $this->module->l('Viljandi maakond', 'payment'),
            ),
            array(
                'value' => 'Võru maakond',
                'text' => $this->module->l('Võru maakond', 'payment'),
            )
        );
        $salary_choices = array(
            array(
                'value' => 'up to 250',
                'text' => $this->module->l('up to 250', 'payment')
            ),
            array(
                'value' => '251-500',
                'text' => $this->module->l('251-500', 'payment')
            ),
            array(
                'value' => '501-700',
                'text' => $this->module->l('501-700', 'payment')
            ),
            array(
                'value' => '701-1000',
                'text' => $this->module->l('701-1000', 'payment')
            ),
            array(
                'value' => '1001-1300',
                'text' => $this->module->l('1001-1300', 'payment')
            ),
            array(
                'value' => 'more than 1300',
                'text' => $this->module->l('more than 1300', 'payment')
            ),
            array(
                'value' => 'no income',
                'text' => $this->module->l('no income', 'payment')
            ),
        );
        $time_with_employer_choices = array(
            array(
                'value' => 'up to 6 months',
                'text' => $this->module->l('up to 6 months', 'payment')
            ),
            array(
                'value' => '6-24 months',
                'text' => $this->module->l('6-24 months', 'payment')
            ),
            array(
                'value' => '24-60 months',
                'text' => $this->module->l('24-60 months', 'payment')
            ),
            array(
                'value' => 'more than 60 months',
                'text' => $this->module->l('more than 60 months', 'payment')
            ),
            array(
                'value' => 'self-employed',
                'text' => $this->module->l('self-employed', 'payment')
            ),
            array(
                'value' => 'unemployed',
                'text' => $this->module->l('unemployed', 'payment')
            ),
            array(
                'value' => 'retired',
                'text' => $this->module->l('retired', 'payment')
            ),
            array(
                'value' => 'parental leave',
                'text' => $this->module->l('parental leave', 'payment')
            ),
        );
		$duration_choices = $this->module->getContractDurations();
	  	$this->context->smarty->assign(array(
            'county_choices' => $county_choices,
            'salary_choices' => $salary_choices,
            'time_with_employer_choices' => $time_with_employer_choices,
            'duration_choices' => $duration_choices,
            'liisi_county_value' => Tools::getValue('liisi_county'),
            'min_downpayment' => $this->getMinDownpayment($this->context->cart->getOrderTotal(true, Cart::BOTH)),
        ));
    }

    private function assignFormValuesFromAddress()
    {
        $invoice_address = new Address($this->context->cart->id_address_invoice, $this->context->language->id);
        $this->context->smarty->assign(
            array(
                'address_firstname' => $invoice_address->firstname,
                'address_surname' => $invoice_address->lastname,
                'address_address1' => $invoice_address->address1,
                'address_phone' => $invoice_address->phone ? $invoice_address->phone : $invoice_address->phone_mobile,
                'address_zip' => $invoice_address->postcode,
                'address_city' => $invoice_address->city,
                'address_email' => $this->context->customer->email
            )
        );
    }

    private function processLiisiRequest($liisi_fields)
    {
        $xml = $this->createRequestXML($liisi_fields);

        include(dirname(__FILE__).'/../../lib/httpful.phar');

        $response = \Httpful\Request::post(Liisi::$liisi_servers[$this->module->config['LIISI_SERVER']]['url'])
            ->clientSideCert(dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'], dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'])
            ->basicAuth($this->module->config['LIISI_USERNAME'], $this->module->config['LIISI_PASSWORD'])
            ->addOnCurlOption(CURLOPT_SSLVERSION, 1)
            ->body($xml->saveXML())
            ->mime(\Httpful\Mime::XML)
            ->send();

        switch ($response->body->Status[0])
        {
            case ('AUTO_SATISFIED'):
            case ('PENDING'):
            case ('AUTO_REJECTED'):
            case ('REJECTED'):
                $this->context->smarty->assign(array(
                    'liisi_status' => $response->body->Status[0],
                    'liisi_response' => $response->body,
                ));
                break;
            case ('ERROR'):
                $this->errors[] = sprintf($this->module->l('Liisi error: %s', 'payment'), $response->body->Comment[0]);
                break;
            default:
                throw new PrestaShopException('Unknown Liisi response status');
        }
    }

    private function processLiisiConfirmation($order_id)
    {
        $xml = "<?xml version='1.0' encoding='utf-8' ?>
                <ConfirmationRequest>
                    <Connection>
                    <Version>4</Version>
                    </Connection>
                    <OrderId>$order_id</OrderId>
                </ConfirmationRequest>";

        include(dirname(__FILE__).'/../../lib/httpful.phar');

        $response = \Httpful\Request::post(Liisi::$liisi_servers[$this->module->config['LIISI_SERVER']]['url'])
            ->clientSideCert(dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'], dirname(__FILE__).'/../../private/'.$this->module->config['LIISI_PRIVATE_KEY'])
            ->basicAuth($this->module->config['LIISI_USERNAME'], $this->module->config['LIISI_PASSWORD'])
            ->addOnCurlOption(CURLOPT_SSLVERSION, 1)
            ->body($xml)
            ->mime(\Httpful\Mime::XML)
            ->send();

        $presta_cart_id = current(explode('-', $order_id));
        $this->module->createOrderWithStatus('LIISI_CONTRACT', $presta_cart_id, array('downpayment' => (string)$response->body->Terms->DownPayment[0]));
    }

    private function processLiisiPending($cart_id)
    {
        $presta_cart_id = current(explode('-', $cart_id));
        $this->module->createOrderWithStatus('LIISI_PENDING', $presta_cart_id);
    }

    private function createRequestXML($fields)
    {
        include dirname(__FILE__).'/../../classes/SimpleXMLExtended.php';

        $cart_rules = $this->context->cart->getCartRules();
        $discount = 0;

        foreach ($cart_rules as $cart_rule)
        {
            $discount += $cart_rule['value_real'];
        }
        $discount = Tools::ps_round($discount, 2);

        $xml = new SimpleXMLExtended(
            '<?xml version="1.0" encoding="utf-8" ?>
            <ApplicationRequest>
                <Connection>
                    <Version></Version>
                </Connection>
                <OrderId></OrderId>
                <Client>
                    <Identified></Identified>
                    <IdCode></IdCode>
                    <Surname></Surname>
                    <FirstName></FirstName>
                    <Phone></Phone>
                    <Email></Email>
                    <Address>
                        <Street></Street>
                        <HouseNr></HouseNr>
                        <Apartment></Apartment>
                        <Locality></Locality>
                        <County></County>
                        <ZipCode></ZipCode>
                    </Address>
                    <PoliticallyExposed></PoliticallyExposed>
                    <Employment>
                        <AllowPensionQuery></AllowPensionQuery>
                        <Employer></Employer>
                        <TimeWithEmployer></TimeWithEmployer>
                        <SalaryAmount></SalaryAmount>
                        <LoanObligations></LoanObligations>
                    </Employment>
                </Client>
                <Products></Products>
                <Payment>
                    <ScheduleDuration></ScheduleDuration>
                    <DownPayment></DownPayment>
                    <PostalFee></PostalFee>
                    <FirstPaymentDate></FirstPaymentDate>
                </Payment>
                <Reply>
                    <URL></URL>
                    <Username></Username>
                    <Password></Password>
                </Reply>
                <ClientIpAddress></ClientIpAddress>
            </ApplicationRequest>');

        $xml->Connection->Version = 4;

        $xml->OrderId = $this->context->cart->id . '-' . time();

        $xml->Client->Identified = 'no';
        $xml->Client->IdCode = $fields['liisi_idcode']['value'];
        $xml->Client->Surname = $fields['liisi_surname']['value'];
        $xml->Client->FirstName = $fields['liisi_firstname']['value'];
        $xml->Client->Phone = $fields['liisi_phone']['value'];
        $xml->Client->Email = $fields['liisi_email']['value'];

        $xml->Client->Address->Street = $fields['liisi_street']['value'];
        $xml->Client->Address->Locality = $fields['liisi_locality']['value'];
        $xml->Client->Address->County = $fields['liisi_county']['value'];
        $xml->Client->Address->ZipCode = $fields['liisi_zipcode']['value'];

        $xml->Client->Employment->AllowPensionQuery = $fields['liisi_allow_pension_query']['value'] == 1 ? 'true' : 'false';
        $xml->Client->Employment->Employer = $fields['liisi_employer']['value'];
        $xml->Client->Employment->TimeWithEmployer = $fields['liisi_timewithemployer']['value'];
        $xml->Client->Employment->SalaryAmount = $fields['liisi_salary']['value'];
        $xml->Client->Employment->LoanObligations = $fields['liisi_loan_obligations']['value'];

        $xml->Client->PoliticallyExposed = $fields['liisi_politically_exposed']['value'] == 1 ? 'true' : 'false';
        $xml->ClientIpAddress = $this->getCustomerIp();

        /*
        $shipping_cost = $this->context->cart->getTotalShippingCost();
        if ($shipping_cost > 0)
        {
            $new_shipping_cost =  max(0, ($shipping_cost - $discount));
            $product = $xml->Products->addChild('Product');
            $product->addChild('Name', $this->module->l('Shipping', 'payment').($discount > 0.00 ? sprintf(' (%01.2f)', $shipping_cost) : ''));
            $product->addChild('Quantity', 1);
            $product->addChild('TotalPrice', $new_shipping_cost);

            if ($discount > 0.00)
            {
                $discount = max(0, $discount - $shipping_cost);
            }
        }
        */

        foreach ($this->context->cart->getProducts() as $cart_product)
        {
            $price = max(0, ($cart_product['total_wt'] - $discount));

            $product = $xml->Products->addChild('Product');
            $product->addChild('Name', $cart_product['name'].($discount > 0.00 ? sprintf(' (%01.2f)', $cart_product['total_wt']) : ''));
            $product->addChild('Quantity', $cart_product['cart_quantity']);
            $product->addChild('TotalPrice', $price);

            if ($discount > 0.00)
            {
                $discount = max(0, $discount - $cart_product['total_wt']);
            }
        }

		$date = new DateTime();
		$date->add(new DateInterval('P2W'));
		if ((int)$fields['liisi_first_payment_date']['value'] < (int)$date->format('d')) {
			$date->add(new DateInterval('P1M'));
		}

        $xml->Payment->ScheduleDuration = $fields['liisi_duration']['value'];
        $xml->Payment->DownPayment = $fields['liisi_downpayment']['value'];
        $xml->Payment->PostalFee = $this->context->cart->getTotalShippingCost();
		$xml->Payment->FirstPaymentDate = $date->format('Y-m-').
			((int)$fields['liisi_first_payment_date']['value'] < 10? 0 : '').
			$fields['liisi_first_payment_date']['value'];

        $xml->Reply->URL = $this->context->link->getModuleLink('liisi', 'reply', array(), true);
		return $xml;
    }

    public function getCustomerIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

}
