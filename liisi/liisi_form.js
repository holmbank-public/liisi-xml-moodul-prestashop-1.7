function submitInputReplace(){
	var liisi_loan_obl = document.getElementById('liisi_loan_obligations').value;
	if (liisi_loan_obl == '0') {
		document.getElementById('liisi_loan_obligations').value = '0.00';
	}
	return true;
}

$(function(){

    $('#liisi_form').submit(function() {
        $(this).find('#liisi_submit_request').hide();
        $(this).find('#request_loading').show();
    });

	$(document).ready(function(){
		$('#liisi_first_payment_date').datepicker({
			dateFormat: "yy-mm-dd"
		});
	});

});
