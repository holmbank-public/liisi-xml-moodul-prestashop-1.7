$(document).ready(function(){
	$('#liisi-test-button').click(function(e){
		e.preventDefault();
		$.ajax({
			url: $(this).attr('data-url'),
			dataType: 'json',
			success: function(data){
				if (data.result)
				{
					$('#liisi-test-result').text(data.result);
					if (data.result == 'success')
					{
						$('#liisi-test-result').css({color: 'green'});
					}
					else
					{
						$('#liisi-test-result').css({color: 'red'});
					}
				}
			}
		});
	});
});
