<script type="text/javascript">
	var complects = new Array('500-999.99,7-12,12.5', '500-999.99,25-60,14', '500-999.99,13-24,13', '500-999.99,1-6,12', '300-499.99,13-36,14.5', '300-499.99,37-60,15', '300-499.99,1-12,16', '150-299.99,7-12,18', '150-299.99,37-60,16', '150-299.99,13-36,17', '150-299.99,1-6,20', '1000-6400,7-12,12', '1000-6400,25-60,13', '1000-6400,13-24,12.5', '1000-6400,1-6,9', '0-149.99,37-60,18', '0-149.99,25-36,20', '0-149.99,13-24,21', '0-149.99,1-12,25');
	function calc_payment() {
		var l_price = new Number(document.getElementById('l_price').value.replace(',', '.'));
		var l_deposit = new Number(document.getElementById('l_deposit').value.replace(',', '.'));
		var months_obj = document.getElementById('l_months');
		if (months_obj.options == undefined) var l_months = new Number(months_obj.value);
		else var l_months = new Number(months_obj.options[months_obj.selectedIndex].value);
		var l_interest = 0;
		for (var i = 0; i < complects.length; i++) {
			if (l_interest == 0) {
				var complect = complects[i].split(',');
				var p_range = complect[0].split('-');
				var m_range = complect[1].split('-');
				if (l_price - l_deposit >= Number(p_range[0]) && l_price - l_deposit <= Number(p_range[1]) && l_months >= Number(m_range[0]) && l_months <= Number(m_range[1])) l_interest = Number(complect[2]);
			}
		}
		if (l_interest == 0 || l_price == 0) {
			document.getElementById('l_result_error').style.display = '';
			document.getElementById('l_result_div').style.display = 'none';
			document.getElementById('l_result_min_error').style.display = 'none';
		}
		else {
			l_interest = l_interest / 1200;
			var m_loan = (l_price - l_deposit) / l_months;
			var m_interest = (l_price - l_deposit) * l_interest;
			var m_payment = m_loan + m_interest;
			m_payment = Math.round(m_payment * 100) / 100;
			if (m_payment < 7) {
				document.getElementById('l_result_min_error').style.display = '';
				document.getElementById('l_result_error').style.display = 'none';
				document.getElementById('l_result_div').style.display = 'none';
			}
			else {
				document.getElementById('l_result_error').style.display = 'none';
				document.getElementById('l_result_min_error').style.display = 'none';
				document.getElementById('l_result_div').style.display = '';
				document.getElementById('l_result').innerHTML = l_months.toString() + ' x ' + m_payment.toString();
			}
		}
	}

	function recalcPrice(qty)
	{
		var qty = qty || 1;
		$('#l_price').val((parseInt(qty) * productPrice).toFixed(2));
		calc_payment();
	}

	$(document).ready(function(){
		if (typeof productPrice === 'number')
		{
			recalcPrice(1);
		}

		$('#quantity_wanted').on('change', function(evt) {
			recalcPrice(parseInt($(this).val()));
		});

		$('#quantity_wanted_p').on('click', '.btn', function(evt) {
			setTimeout(function() {
				recalcPrice(parseInt($('#quantity_wanted').val()));
			});
		});
	});
</script>
<style>
	#liisi-calculator {
		margin-top: 15px;
	}
	#liisi-calculator h3 {
		padding-bottom: 0px;
	}
	#liisi-calculator legend {
		color: #ff6600;
		font-weight: bold;
		font-size: 1.1em;
		line-height: 2.5em;
		width: auto;
		border: none;
		padding-left: 5px;
		padding-right: 5px;
	}
	#liisi-calculator fieldset.calculator legend {
		margin-bottom: 10px;
	}
	#liisi-logo {
		width: 50px;
		position: relative;
		top: -3px;
	}
	#liisi-calculator fieldset {
		border: #5b90ab solid 1px;
		padding: 0 5px 10px 5px;
		margin-bottom: 15px;
		max-width: 330px;
		text-align: center;
	}
	#liisi-calculator table {
		width: 100%;
	}
	#liisi-calculator table td {
		padding: 5px;
	}
	#liisi-calculator table td:last-child {
		text-align: right;
	}
	#liisi-calculator label {
	color: #65788e;
	}
	#liisi-calculator input[type="text"] {
		width: 100%;
	}
	#liisi-calculator INPUT, #liisi-calculator TEXTAREA, #liisi-calculator SELECT {
		padding: 1px;
		border: #5b90ab solid 1px;
	}
	#liisi-calculator .button {
		color: #ff6600;
		font-weight: bold;
		padding: 5px 15px;
		border: #5b90ab solid 1px;
		background-color: #DEF1FB;
		cursor: pointer;
		background-image: none;
	}
	#l_result_error, #l_result_min_error {
		color: red;
	}
	#l_result_div {
		color: green;
	}
	#liisi-calculator fieldset.result {
		margin-bottom: 10px;
	}
</style>
<div id="liisi-calculator"> 
	
	<fieldset class="calculator"> 
		<legend><img src="{$base_dir}modules/liisi/liisi_display_payment.png" id="liisi-logo" alt="Liisi" title="Liisi" /> kalkulaator</legend>
			<table>
														<tr>
						<td class="left"><label>Kauba hind (EUR): </label></td>
						<td class="right">
													<input type="text" id="l_price" />
												</td>
					</tr>
																			<tr>
						<td class="left"><label>Sissemakse (EUR): </label></td>
						<td class="right">
													<input type="text" id="l_deposit" />
												</td>
					</tr>
																			<tr>
						<td class="left"><label>Kuude arv: </label></td>
						<td class="right">
													<select id="l_months">
															<option value="3">3 kuud</option>
															<option value="6">6 kuud</option>
															<option value="12">12 kuud</option>
															<option value="18">18 kuud</option>
															<option value="24">24 kuud</option>
															<option value="30">30 kuud</option>
															<option value="36">36 kuud</option>
															<option value="42">42 kuud</option>
															<option value="48">48 kuud</option>
															<option value="54">54 kuud</option>
															<option value="60">60 kuud</option>
														</select>
												</td>
					</tr>
																						<tr>
					<td colspan="2" style="text-align: center;">
						<input class="button" type="button" value="Arvuta kuumakse" onfocus="this.blur();" onclick="calc_payment();" />
					</td>
				</tr>
			</table>
			<table>
					<tr>
						<td class="left">
							<label>Kuumaksed:</label>
						</td>
						<td>
							<div id="l_result_div" style="display: none;"><span id="l_result"></span>&nbsp;EUR</div>
							<div id="l_result_error" style="display: none;">Midagi läks valesti!</div>
							<div id="l_result_min_error" style="display: none;">Minimaalne kuumakse on 7 eurot!</div>
						</td>
					</tr>
			</table>
	</fieldset> 
	
</div>
<!-- <iframe
        height="{if $type === 'no_text'}280{else}650{/if}"
        src="http://www.liisi.ee/calculator.php?type=calc_{$type}"
        frameborder="0"
        width="100%"
        scrolling="no"
        {if $type === 'text'}
        name="trailer_content"
        onload="this.height=parseInt(parent.trailer_content.document.body.scrollHeight)"
        {/if}
></iframe> -->
