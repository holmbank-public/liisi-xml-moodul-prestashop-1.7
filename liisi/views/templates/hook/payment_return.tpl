{if $status === 'PENDING'}
    <p>{l s='Your Liisi request is pending. You\'ll be contacted with the decision shortly.' mod='liisi'}</p>
{elseif $status === 'ACCEPTED'}
    <p>{l s='Your Liisi request was approved. You\'ll be contacted with the information about your downpayment.' mod='liisi'}</p>
{/if}