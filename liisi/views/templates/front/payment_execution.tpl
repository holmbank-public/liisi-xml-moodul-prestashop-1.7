{capture name=path}{l s='Liisi payment.' mod='liisi'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

{include file="$tpl_dir./errors.tpl"}

{assign var='current_step' value='payment'}

{if !empty($liisi_response)}
    {if $liisi_response->Status == 'AUTO_SATISFIED'}
        <h2>{l s='Lepingu sõlmimine' mod='liisi'}</h2>
        <p class="info">{l s="Your application was automatically aproved. Pleache check and confirm the terms and information" mod='liisi'}</p>
        <p><strong>{l s="Contract Fee:" mod='liisi'}</strong> {$liisi_response->Terms->ContractFee}</p>
        <p><strong>{l s="Monthly Payment:" mod='liisi'}</strong> {$liisi_response->Terms->MonthlyPayment}</p>
        <p><strong>{l s="Duration:" mod='liisi'}</strong> {$liisi_response->Terms->ScheduleDuration} {l s="months" mod='liisi'}</p>
        <p><strong>{l s="Downpayment:" mod='liisi'}</strong> {$liisi_response->Terms->DownPayment}</p>
        {if !empty($liisi_response->Terms->ExpenseRate)}
        <p><strong>{l s="Expense Rate:" mod='liisi'}</strong> {$liisi_response->Terms->ExpenseRate}%</p>
    {/if}
        <form id="liisi_form" action="" method="post">
			<input type="hidden" name="liisi_response" value='{$liisi_response|json_encode}' />
            <input type="hidden" name="liisi_order_id" value="{$liisi_response->OrderId}">
            <p>
                <span style="display:none" id="request_loading">{l s="Loading" mod='liisi'} <img src="{$img_ps_dir}loader.gif" /></span>
                <input id="liisi_submit_request" type="submit" name="liisi_confirm_contract" class="exclusive button" value="{l s='Kinnitan andmete õigsust' mod='liisi'}">
            </p>
        </form>
    {elseif $liisi_response->Status == 'PENDING'}
        <h2>{l s='Järelmaksu taotluse vastus' mod='liisi'}</h2>
        <p class="info">{l s="Your application requires further review." mod='liisi'}
        <br/>{l s="When confirming your order, you will be contacted by e-mail." mod='liisi'}</p>
        <form id="liisi_form" action="" method="post">
			<input type="hidden" name="liisi_response" value='{$liisi_response|json_encode}' />
            <input type="hidden" name="liisi_order_id" value="{$liisi_response->OrderId}">
            <p>
                <span style="display:none" id="request_loading">{l s="Loading" mod='liisi'} <img src="{$img_ps_dir}loader.gif" /></span>
                <input id="liisi_submit_request" type="submit" name="liisi_confirm_pending" class="exclusive button" value="{l s='Confirm order' mod='liisi'}">
            </p>
        </form>
        <hr>
        <p class="info">{l s="To choose another payment method or fill out a new application, please go back your shopping cart." mod='liisi'}</p>
        <p class="cart_navigation"><a href="{$link->getPageLink('order-opc', true)}" class="button_large">{l s='< Back to shopping cart' mod='liisi'}</a></p>
    {elseif $liisi_response->Status == 'AUTO_REJECTED' || $liisi_response->Status == 'REJECTED'}
        <h2>{l s='Järelmaksu taotluse vastus' mod='liisi'}</h2>
        <p class="info">{l s='Your Liisi request was denied. Try paying with other payment methods.' mod='liisi'}</p>
        <p><a href="{$link->getPageLink('order-opc', true)}" >{l s='< Back to cart' mod='liisi'}</a></p>
    {/if}
{else}
    <form id="liisi_form" action="" method="post" onsubmit="submitInputReplace();">
        <p class="required{if !empty($field_data.liisi_firstname.error)} error{/if}">
            <label for="liisi_firstname">{l s='Eesnimi' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_firstname" name="liisi_firstname" value="{if isset($address_firstname)}{$address_firstname}{elseif isset($smarty.post.liisi_firstname)}{$smarty.post.liisi_firstname}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_surname.error)} error{/if}">
            <label for="liisi_surname">{l s='Perekonnanimi' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_surname" name="liisi_surname" value="{if isset($address_surname)}{$address_surname}{elseif isset($smarty.post.liisi_surname)}{$smarty.post.liisi_surname}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_email.error)} error{/if}">
            <label for="liisi_email">{l s='E-post' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_email" name="liisi_email" value="{if isset($address_email)}{$address_email}{elseif isset($smarty.post.liisi_email)}{$smarty.post.liisi_email}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_idcode.error)} error{/if}">
            <label for="liisi_idcode">{l s='Isikukood' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_idcode" name="liisi_idcode" value="{if isset($smarty.post.liisi_idcode)}{$smarty.post.liisi_idcode}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_phone.error)} error{/if}">
            <label for="liisi_phone">{l s='Telefon' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_phone" name="liisi_phone" value="{if isset($address_phone)}{$address_phone}{elseif isset($smarty.post.liisi_phone)}{$smarty.post.liisi_phone}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_county.error)} error{/if}">
            <label for="liisi_county">{l s='Maakond' mod='liisi'}<em>*</em></label>
            <select class="required-entry input-text" id="liisi_county" name="liisi_county">
				<option value=""></option>
                {foreach $county_choices as $county_choice}
                    <option value="{$county_choice.value}" {if isset($smarty.post.liisi_county) && $liisi_county_value == $county_choice.value}selected{/if}>{$county_choice.text}</option>
                {/foreach}
            </select>
        </p>
        <p class="required{if !empty($field_data.liisi_locality.error)} error{/if}">
            <label for="liisi_locality">{l s='Linn/asula' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_locality" name="liisi_locality" value="{if isset($address_city)}{$address_city}{elseif isset($smarty.post.liisi_locality)}{$smarty.post.liisi_locality}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_street.error)} error{/if}">
            <label for="liisi_street">{l s='Aadress' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_street" name="liisi_street" value="{if isset($address_address1)}{$address_address1}{elseif isset($smarty.post.liisi_street)}{$smarty.post.liisi_street}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_zipcode.error)} error{/if}">
            <label for="liisi_zipcode">{l s='Postiindeks' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_zipcode" name="liisi_zipcode" value="{if isset($address_zip)}{$address_zip}{elseif isset($smarty.post.liisi_zipcode)}{$smarty.post.liisi_zipcode}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_duration.error)} error{/if}">
            <label for="liisi_duration">{l s='Lepingu periood' mod='liisi'}<em>*</em></label>
            <select id="liisi_duration" name="liisi_duration">
                <option></option>
                {html_options options=$duration_choices selected=$smarty.post.liisi_duration}
            </select>
        </p>
        <p class="required{if !empty($field_data.liisi_downpayment.error)} error{/if}">
            <label for="liisi_downpayment">{l s='Sissemakse (EUR)' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_downpayment" name="liisi_downpayment" value="{if isset($smarty.post.liisi_downpayment)}{$smarty.post.liisi_downpayment}{/if}">
            <br/>
            <span id="liisi_order_total"><strong>{l s='Order total:' mod='liisi'}</strong> {displayPrice price=$cart->getOrderTotal()}</span>
            <span id="liisi_min_downpayment"><strong>{l s='Minimum downpayment:' mod='liisi'}</strong> {displayPrice price=$min_downpayment}</span>
        </p>
        <p class="required{if !empty($field_data.liisi_first_payment_date.error)} error{/if}">
            <label for="liisi_first_payment_date">{l s='Makse kuupäev' mod='liisi'}<em>*</em></label>
			<select class="required-entry input-text" id="liisi_first_payment_date" name="liisi_first_payment_date">
				<option></option>
				{for $i=1 to 31}
					<option value="{$i}"
						{if isset($smarty.post.liisi_first_payment_date) && $smarty.post.liisi_first_payment_date == $i}selected{/if}
					>{$i}</option>
				{/for}
			</select>
        </p>
        <p class="required{if !empty($field_data.liisi_employer.error)} error{/if}">
            <label for="liisi_employer">{l s='Tööandja' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_employer" name="liisi_employer" value="{if isset($smarty.post.liisi_employer)}{$smarty.post.liisi_employer}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_timewithemployer.error)} error{/if}">
            <label for="liisi_timewith">{l s='Tööandja juures töötatud aeg' mod='liisi'}<em>*</em></label>
            <select id="liisi_timewith" name="liisi_timewithemployer" class="validate-select">
				<option value=""></option>
                {foreach $time_with_employer_choices as $time_with_employer_choice}
                    <option value="{$time_with_employer_choice.value}" {if isset($smarty.post.liisi_timewithemployer) && $smarty.post.liisi_timewithemployer === $time_with_employer_choice.value}selected{/if}>{$time_with_employer_choice.text}</option>
                {/foreach}
            </select>
        </p>
        <p class="required{if !empty($field_data.liisi_salary.error)} error{/if}">
            <label for="liisi_salary">{l s='Töötasu' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_salary" name="liisi_salary" value="{if isset($smarty.post.liisi_salary)}{$smarty.post.liisi_salary}{/if}">
        </p>
        <p class="required{if !empty($field_data.liisi_loan_obligations.error)} error{/if}">
            <label for="liisi_salary">{l s='Kohustused' mod='liisi'}<em>*</em></label>
            <input class="required-entry input-text" type="text" id="liisi_loan_obligations" name="liisi_loan_obligations" value="{if isset($smarty.post.liisi_loan_obligations)}{$smarty.post.liisi_loan_obligations}{/if}">
        </p>

        <p class="required{if !empty($field_data.liisi_allow_pension_query.error)} error{/if}">
            <label for="liisi_allow_pension_query">{l s='Tee Maksu- ja Tolliameti päring' mod='liisi'}<em>*</em></label>
            <input type="checkbox" name="liisi_allow_pension_query" value="1">
			{* <select id="liisi_allow_pension_query" name="liisi_allow_pension_query" class="validate-select">
		
                <option value="0" {if isset($smarty.post.liisi_allow_pension_query)}selected{/if}>{l s='Ei'}</option>
                <option value="1" {if isset($smarty.post.liisi_allow_pension_query)}selected{/if}>{l s='Jah'}</option>
            </select> *}

            <span class="tooltip">
            <i class="icon-question-sign help tooltip"></i>
                <span class="tooltiptext">
                    {l s='Krediidiotsuse tegemiseks luban Maksu- ja Tolliametil teha päringu ja edastada minu päringu andmed Holm Bank AS-le.

Kui lubad Holm Bank AS-il pärida ja Maksu- ja Tolliametil edastada alltoodud andmeid, siis kiirendab see Sinu taotluse analüüsi ja krediidiotsuse tegemist. Koostame päringu nõusoleku andmisele eelneva 12 kuu kohta. Maksu- ja Tolliamet edastab Holm Bank AS-ile järgmised andmed:

• deklareeritud tulu (v.a tulu vara võõrandamisest);

• töövõimetus-, töötuskindlustus- ja koondamishüvitised, pensionid, sissemaksed III pensionisambasse, kogumispensioni väljamaksed (TSD alusel);

• töötamise registrisse tehtud kanded (info tööandja ja töösuhte kohta);

• dividendid ja omakapitalist tehtud väljamaksed;

• maksuvõlg alates 100 €.

Holm Bank AS kasutab saadud andmeid krediidivõimelisuse hindamise eesmärgil. Holm Bank AS-il on õigus teha päringuid Maksu- ja Tolliametisse üksnes krediiditaotluse menetlemise ajal. Nõusolek on võimalik tagasi võtta igal ajal, teavitades sellest Holm Bank AS-i.Andmete töötlemise kohta Maksu- ja Tolliametis tutvu Andmetöötlustingimustega nende kodulehel. Maksuandmete alaste küsimustega palun pöördu Maksu- ja Tolliameti poole infotelefonil 880 0811.' mod='liisi'}
                </span>
            </span>
        </p>

        <p class="required{if !empty($field_data.liisi_politically_exposed.error)} error{/if}">
            <label for="liisi_politically_exposed">{l s='Riikliku taustaga isik' mod='liisi'}<em>*</em></label>
            <select id="liisi_politically_exposed" name="liisi_politically_exposed" class="validate-select">
                <option value="0" {if isset($smarty.post.liisi_politically_exposed)}selected{/if}>{l s='Ei'}</option>
                <option value="1" {if isset($smarty.post.liisi_politically_exposed)}selected{/if}>{l s='Jah'}</option>
            </select>

            <span class="tooltip">
            <i class="icon-question-sign help tooltip"></i>
                <span class="tooltiptext">
                    {l s='Kas Teie, Teie perekonnaliige või lähedane kaastöötaja on riikliku taustaga isik (kõrgeima avaliku võimu ülesannete täitja)?
    Riikliku taustaga isik on füüsiline isik, kes täidab või on täitnud viimase aasta jooksul avaliku võimu olulisi ülesandeid, samuti sellise isiku perekonnaliikmed ja lähedased kaastöötajad.
    Avaliku võimu oluliste ülesannete täitmiseks loetakse töötamist rahapesu ja terrorismi rahastamise seaduse § 20 lõikes 1 nimetatud kõrgematel riiklikel ametikohtadel või riigi äriühingu juhtimis-, järelevalve- ja haldusorgani liikmena.' mod='liisi'}
                </span>
            </span>
        </p>
        <p>
            <span style="display:none" id="request_loading">{l s="Loading" mod='liisi'} <img src="{$img_ps_dir}loader.gif" /></span>
            <input id="liisi_submit_request" type="submit" name="liisi_submit_request" class="exclusive button" value="{l s='Submit' mod='liisi'}"/>
        </p>
    </form>
{/if}
