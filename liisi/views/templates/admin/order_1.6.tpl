<div class="panel col-lg-6">
	<div class="panel-heading"><i class="icon-euro"></i> </span>LIISI</div>
	<table class="table">
		{foreach from=$liisi key='key' item='val'}
			{if $key != 'id' && $key != 'id_order' && $key != 'id_shop_list' && $key != 'force_id'}
				<tr><td><strong>{$key}</strong></td><td>{$val}</td></tr>
			{/if}
		{/foreach}
	</table>
</div>
