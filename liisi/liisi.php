<?php

include 'classes/LiisiModel.php';

if (!defined('_PS_VERSION_'))
    exit;

class Liisi extends PaymentModule
{
    public $local_path;
    public $_path;

    public $config;

    public static $liisi_servers = array(
        array(
            'id' => 0,
            'url' => 'https://pood.liisi.ee:9300/webshop/application/',
            'name' => 'Liisi (Estonia) LIVE'
        ),
        array(
            'id' => 1,
            'url' => 'https://prelive.liisi.ee:9300/webshop/application/',
            'name' => 'Liisi (Estonia) TEST'
        )
    );

    private static $CONFIG_KEYS = array(
        'LIISI_USERNAME',
        'LIISI_PASSWORD',
        'LIISI_PRIVATE_KEY',
        'LIISI_SERVER',
        'LIISI_MIN_DOWNPAYMENT_PERCENTAGE',
        'LIISI_MIN_DOWN_PAYMENT_SUM',
        'LIISI_CONTRACT_LENGTH'
    );

    public function __construct()
    {
        $this->name = 'liisi';
        $this->tab = 'payments_gateways';
        $this->version = '1.3.0';
        $this->author = 'veebipoed.ee';

        parent::__construct();

        $this->displayName = $this->l('Liisi');
        $this->description = $this->l('Adds an ability to accept Liisi lease requests.');

        $this->refreshConfiguration();

        $this->bootstrap = true;
    }

    public function install()
    {
		$sql = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'liisi (
			id_liisi INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			id_order INT UNSIGNED NOT NULL,
			contract_number varchar(64) NOT NULL,
			contract_fee varchar(32),
			down_payment varchar(32),
			postal_fee varchar(32),
			monthly_payment varchar(32),
			schelude_duration varchar(32),
			id_code varchar(32),
			surname varchar(64),
			firstname varchar(64),
			phone varchar(32),
			email varchar(128),
			address varchar(255),
			employer varchar(64),
			time_with_employer varchar(32),
			salary varchar(32)
		)';
        return parent::install()
        && Db::getInstance()->execute($sql)
        && $this->registerHook('displayRightColumnProduct')
        && $this->registerHook('displayPayment')
        && $this->registerHook('displayPaymentReturn')
        && $this->registerHook('displayAdminOrder')
        && $this->createOrderStatuses();
    }

    public function uninstall()
    {
        $order_state_ids = Configuration::getMultiple(array('LIISI_PENDING', 'LIISI_CONTRACT', 'LIISI_DENIED'));
        foreach ($order_state_ids as $id_order_state) {
            $state = new OrderState($id_order_state);
            $state->delete();
        }

        /*foreach (Liisi::$CONFIG_KEYS as $config_name) {
            Configuration::deleteByName($config_name);
        }*/

        return parent::uninstall();
    }

    public function getContent()
	{
		$this->context->controller->addJS($this->_path.'views/js/test_button.js');

        $output = null;

        if (Tools::isSubmit('submit'.$this->name)) {
            $id_server = intval(Tools::getValue('LIISI_SERVER'));
            if (is_numeric(Tools::getValue('LIISI_SERVER')) && array_key_exists($id_server, Liisi::$liisi_servers)) {
                Configuration::updateValue('LIISI_SERVER', $id_server);
            } else {
                $output .= $this->displayError($this->l('Invalid Server value'));
            }

            $username = Tools::getValue('LIISI_USERNAME');
            if (!empty($username)) {
                Configuration::updateValue('LIISI_USERNAME', $username);
            } else {
                $output .= $this->displayError($this->l('Invalid Username'));
            }

            $password = Tools::getValue('LIISI_PASSWORD');
            if (!empty($password)) {
                Configuration::updateValue('LIISI_PASSWORD', $password);
            } else if (empty($this->config['LIISI_PASSWORD'])) {
                $output .= $this->displayError($this->l('Invalid Password'));
            }

            /*$client_cert = isset($_FILES['LIISI_CLIENT_CERT']) ? $_FILES['LIISI_CLIENT_CERT'] : null;
            if (!empty($client_cert['name'])) {
                $file_info = new finfo(FILEINFO_MIME_TYPE);
                $file_mime_type = $file_info->buffer($client_cert['tmp_name']);
                $new_name = sha1_file($client_cert['tmp_name']).'.pem';
                if ($file_mime_type === 'text/plain') {
                    if (
                        is_uploaded_file($client_cert['tmp_name']) &&
                        move_uploaded_file($client_cert['tmp_name'], $this->local_path.'private/'.$new_name)
                    ) {
                        Configuration::updateValue('LIISI_CLIENT_CERT', $new_name);
                    } else {
                        $output .= $this->displayError($this->l('Problem with uploaded Client Certificate'));
                    }
                } else {
                    $output .= $this->displayError($this->l('Invalid file format for Client Certificate'));
                }
            } else if (empty($this->config['LIISI_CLIENT_CERT'])) {
                $output .= $this->displayError($this->l('Missing Client Certificate'));
            }*/

            $client_cert_key = isset($_FILES['LIISI_PRIVATE_KEY']) ? $_FILES['LIISI_PRIVATE_KEY'] : null;
            if (!empty($client_cert_key['name'])) {
                $file_info = isset($file_info) ? $file_info : new finfo(FILEINFO_MIME_TYPE);
                $file_mime_type = $file_info->buffer($client_cert_key['tmp_name']);
                $new_name = sha1_file($client_cert_key['tmp_name']).'.pem';
                if ($file_mime_type === 'text/plain') {
                    if (
                        is_uploaded_file($client_cert_key['tmp_name']) &&
                        move_uploaded_file(
                            $client_cert_key['tmp_name'],
                            $this->local_path.'private/'.$new_name
                        )
                    ) {
                        Configuration::updateValue('LIISI_PRIVATE_KEY', $new_name);
                    } else {
                        $output .= $this->displayError($this->l('Problem with uploaded Private Key'));
                    }
                } else {
                    $output .= $this->displayError($this->l('Invalid file format for Private Key'));
                }
            } else if (empty($this->config['LIISI_PRIVATE_KEY'])) {
                $output .= $this->displayError($this->l('Missing Private Key'));
            }

            /*$private_key_password = Tools::getValue('LIISI_PRIVATE_KEY_PASSWORD');
            if (!empty($private_key_password)) {
                Configuration::updateValue('LIISI_PRIVATE_KEY_PASSWORD', $private_key_password);
            }*/

            $downpayment_percentage = floatval(Tools::getValue('LIISI_MIN_DOWNPAYMENT_PERCENTAGE'));
            Configuration::updateValue('LIISI_MIN_DOWNPAYMENT_PERCENTAGE', $downpayment_percentage);

            $downpayment_sum = floatval(Tools::getValue('LIISI_MIN_DOWN_PAYMENT_SUM'));
            Configuration::updateValue('LIISI_MIN_DOWN_PAYMENT_SUM', $downpayment_sum);

            $contract = Tools::getValue('LIISI_CONTRACT_LENGTH');
            if (!empty($contract)) {
                Configuration::updateValue('LIISI_CONTRACT_LENGTH', $contract);
            } else {
                $output .= $this->displayError($this->l('Invalid Contract Length'));
            }
        }

        return $output . $this->displayForm();
    }

    public function hookDisplayLeftColumn(array $params)
    {
        return $this->displayCalculator();
    }

    public function hookDisplayRightColumn(array $params)
    {
        return $this->displayCalculator();
    }

	public function hookDisplayRightColumnProduct()
	{
		return $this->displayCalculator();
	}

    public function hookDisplayPayment(array $params)
    {
        $this->smarty->assign('payment_logo', $this->_path.'liisi_display_payment.png');
        return $this->display(__FILE__, 'display_payment.tpl');
    }

    public function hookDisplayPaymentReturn($params)
    {
        if (!$this->active) {
            return;
        }

        $state = $params['objOrder']->getCurrentState();
        if ($state == Configuration::get('LIISI_PENDING')) {
            $this->smarty->assign('status', 'PENDING');
            $this->smarty->assign('order', $params['objOrder']);
        } else if ($state == Configuration::get('LIISI_CONTRACT')) {
            $this->smarty->assign('status', 'ACCEPTED');
        } else if ($state == Configuration::get('LIISI_DENIED')) {
            $this->smarty->assign('status', 'DENIED');
            $this->smarty->assign('order', $params['objOrder']);
        }

        return $this->display(__FILE__, 'payment_return.tpl');
    }

	public function hookDisplayAdminOrder($params)
	{
		$order = new Order($params['id_order']);
		if ($order->module == $this->name && $liisi = LiisiModel::getByOrderID($params['id_order']))
		{
			$version = explode('.', _PS_VERSION_);
			$version_part = '1.5';
			if ($version[0] == 1 && $version[1] > 5)
			{
				$version_part = '1.6';
			}
			$template = $this->context->smarty->createTemplate(_PS_MODULE_DIR_.$this->name.'/views/templates/admin/order_'.$version_part.'.tpl');
			$template->assign(array(
				'liisi' => $liisi
			));
			return $template->fetch();
		}
	}


    public function createOrderWithStatus($response, $cart_id, array $extra_information = array())
    {
        $this->validateOrder($cart_id, Configuration::get($response), $this->context->cart->getOrderTotal(), $this->displayName);

		if ( Tools::getValue('liisi_response') && $response = json_decode(Tools::getValue('liisi_response')) )
		{
			$liisi = new LiisiModel();
			$liisi->id_order            = (int)$this->currentOrder;
			$liisi->contract_number     = $response->OrderId;
			$liisi->contract_fee        = $response->Terms->ContractFee;
			$liisi->down_payment        = $response->Payment->DownPayment;
			$liisi->postal_fee          = $response->Payment->PostalFee;
			$liisi->monthly_payment     = $response->Terms->MonthlyPayment;
			$liisi->schelude_duration   = $response->Terms->ScheduleDuration;
			$liisi->id_code             = $response->Client->IdCode;
			$liisi->surname             = $response->Client->Surname;
			$liisi->firstname           = $response->Client->FirstName;
			$liisi->phone               = $response->Client->Phone;
			$liisi->email               = $response->Client->Email;
			$liisi->address             = $response->Client->Address->Street.', '.
			                              $response->Client->Address->Locality.', '.
			                              $response->Client->Address->ZipCode.', '.
			                              $response->Client->Address->County;
			$liisi->employer            = $response->Client->Employment->Employer;
			$liisi->time_with_employer  = $response->Client->Employment->TimeWithEmployer;
			$liisi->salary              = $response->Client->Employment->SalaryAmount;
            $liisi->loan_obligations    = $response->Client->Employment->LoanObligations;
			$liisi->save();
		}

        switch ($response) {
            case 'LIISI_CONTRACT':
                $downpayment_text = sprintf($this->l('Please pay your downpayment - %s'), $extra_information['downpayment']);
                Mail::Send(
                    $this->context->language->id,
                    'liisi_contract',
                    $this->l('Liisi contract confirmed'),
                    array(
                        '{downpayment_text}' => !empty($extra_information['downpayment']) ? $downpayment_text : false
                    ),
                    $this->context->customer->email,
                    $this->context->customer->firstname . $this->context->customer->lastname,
                    null,
                    null,
                    null,
                    null,
                    $this->local_path . 'mails/'
                );
                break;
            case 'LIISI_PENDING':
                Mail::Send(
                    $this->context->language->id,
                    'liisi_pending',
                    $this->l('Liisi application pending'),
                    array(),
                    $this->context->customer->email,
                    $this->context->customer->firstname . $this->context->customer->lastname,
                    null,
                    null,
                    null,
                    null,
                    $this->local_path . 'mails/'
                );
        }

        Tools::redirect(
            'index.php?controller=order-confirmation&id_cart='.$this->context->cart->id.
            '&id_module='.$this->id.'&id_order='.$this->currentOrder.
            '&key='.$this->context->customer->secure_key
        );
    }

    private function displayCalculator($text = false, $show_everywhere = false)
    {
		if (!$this->context->controller instanceof ProductController)
			return '';

        $type_name = $text ? 'text' : 'no_text';

        if (!$this->isCached('calculator.tpl', $this->getCacheId($type_name))) {
            $this->smarty->assign('type', $type_name);
        }

        return $this->display(__FILE__, 'calculator.tpl', $this->getCacheId($type_name));
    }

    private function displayForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $fields_form = array();
        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Server Settings')
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Server'),
                    'name' => 'LIISI_SERVER',
                    'required' => true,
                    'options' => array(
                        'id' => 'id',
                        'name' => 'name',
                        'query' => Liisi::$liisi_servers
                    )
                ),
				array(
					'type' => 'free',
                    'label' => $this->l('Test connection'),
					'name' => 'LIISI_TEST_CONNECTION'
				)
            )
        );

        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Authentication Settings')
            ),
            'description' => $this->l('Do not update the password and file fields if you want to keep already saved values'),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Username'),
                    'name' => 'LIISI_USERNAME',
                    'required' => true
                ),
                array(
                    'type' => 'password',
                    'label' => $this->l('Password'),
                    'name' => 'LIISI_PASSWORD',
                    'size' => null, // Prestashop 1.5 uses size in password field, without checking if it's set. We'll set it to 'null' as a workaraound.
                    'required' => empty($this->config['LIISI_PASSWORD'])
                ),
                // array(
                //     'type' => 'file',
                //     'label' => $this->l('Client certificate'),
                //     'name' => 'LIISI_CLIENT_CERT',
                //     'required' => empty($this->config['LIISI_CLIENT_CERT'])
                // ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Private key'),
                    'name' => 'LIISI_PRIVATE_KEY',
                    'required' => empty($this->config['LIISI_PRIVATE_KEY'])
                ),
                // array(
                //     'type' => 'password',
                //     'label' => $this->l('Private key password'),
                //     'name' => 'LIISI_PRIVATE_KEY_PASSWORD',
                //     'required' => false
                // )
            )
        );

        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Liisi terms')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimal down payment (%)'),
                    'name' => 'LIISI_MIN_DOWNPAYMENT_PERCENTAGE',
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Minimal down payment (sum)'),
                    'name' => 'LIISI_MIN_DOWN_PAYMENT_SUM',
                    'required' => false
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Contract length'),
                    'name' => 'LIISI_CONTRACT_LENGTH',
                    'required' => true,
                    'desc' => $this->l('Contract duration values separated by comma. For instance: 6,12,18')
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->title = $this->displayName;

        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                        '&token='.Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        $this->refreshConfiguration();

        $helper->fields_value['LIISI_USERNAME'] = $this->config['LIISI_USERNAME'];
        // $helper->fields_value['LIISI_PRIVATE_KEY_PASSWORD'] = $this->config['LIISI_PRIVATE_KEY_PASSWORD'];
        $helper->fields_value['LIISI_SERVER'] = $this->config['LIISI_SERVER'];
        $helper->fields_value['LIISI_MIN_DOWNPAYMENT_PERCENTAGE'] = $this->config['LIISI_MIN_DOWNPAYMENT_PERCENTAGE'];
        $helper->fields_value['LIISI_MIN_DOWN_PAYMENT_SUM'] = $this->config['LIISI_MIN_DOWN_PAYMENT_SUM'];
        $helper->fields_value['LIISI_CONTRACT_LENGTH'] = $this->config['LIISI_CONTRACT_LENGTH'];
		$test_button_template = $this->context->smarty->createTemplate(_PS_MODULE_DIR_.$this->name.'/views/templates/parts/test_button.tpl');
		$ssl = isset($_SERVER['HTTPS']);
		$test_button_template->assign(array(
			'test_connection_link' => $this->context->link->getModuleLink($this->name, 'test', array(), $ssl)
		));
		$helper->fields_value['LIISI_TEST_CONNECTION'] = $test_button_template->fetch();

        return $helper->generateForm($fields_form);
    }

    private function createOrderStatuses()
    {
        $states = array(
            'LIISI_PENDING' => array(
                'name' => 'Awaiting Liisi response',
                'color' => '#ffff00',
                'template' => 'liisi_pending',
                'send_email' => 1,
            ),
            'LIISI_CONTRACT' => array(
                'name' => 'Liisi Contract Confirmed',
                'color' => '#00ff00',
                'template' => 'liisi_contract',
                'send_email' => 1,
            ),
            'LIISI_DENIED' => array(
                'name' => 'Liisi Application Denied',
                'color' => '#ff0000',
                'template' => 'liisi_denied',
                'send_email' => 1,
            )
        );
        $languages = Language::getLanguages();

        foreach ($states as $conf => $fields) {
            $order_state = new OrderState(null, $this->context->language->id);
            $order_state->name = array();
            foreach ($languages as $lang) {
                $order_state->name[$lang['id_lang']] = $fields['name'];
            }
            $order_state->color = $fields['color'];
            if ($fields['send_email'] === 1) {
                $order_state->send_email = 1;
                $order_state->template = $fields['template'];
            }
            $order_state->module_name = $this->name;

            $order_state->add();
            Configuration::updateValue($conf, $order_state->id);

			$file = $this->getLocalPath().'img/'.$conf.'.gif';
			$newfile = _PS_IMG_DIR_.'os/' . $order_state->id . '.gif';
			$logoCopied = (is_file($newfile) || (!is_file($newfile) && copy($file, $newfile)));
        }

        $this->recursiveCopy(
            $this->getLocalPath() . 'mails/',
            _PS_MAIL_DIR_,
            Language::getLanguages(true)
        );

        return true;
    }

    private function recursiveCopy($src, $dest, $languages)
    {
        $result = true;
        $allowedExtension = array('html', 'txt');
        $isoCodes = array();

        foreach ($languages as $lang) {
            $isoCodes[] = $lang['iso_code'];
        }

        foreach (Tools::scandir($src, false, '', true) as $file) {
            $pathinfo = pathinfo($file);
            if (
                $pathinfo['extension'] &&
                in_array($pathinfo['extension'], $allowedExtension) &&
                $pathinfo['dirname'] &&
                in_array($pathinfo['dirname'], $isoCodes)
            ) {
                @copy($src.$file, $dest.$file);
            }

        }

        return $result;
    }

    public function getContractDurations()
    {
        $contract_length = explode(',', $this->config['LIISI_CONTRACT_LENGTH']);
        if ($contract_length) {
            $result = array();
            foreach ($contract_length as $duration) {
                $result[(int)$duration] = (int)$duration;
            }
        } else {
            $result = array(
                6 => 6,
                12 => 12,
                24 => 24,
                36 => 36,
                48 => 48,
            );
        }

        return $result;
    }

    private function refreshConfiguration()
    {
        $this->config = Configuration::getMultiple(Liisi::$CONFIG_KEYS);
    }
}
