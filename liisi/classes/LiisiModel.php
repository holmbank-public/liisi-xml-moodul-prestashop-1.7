<?php

class LiisiModel extends ObjectModel
{
	public $id_order;
	public $contract_number;
	public $contract_fee = '';
	public $down_payment = '';
	public $postal_fee = '';
	public $monthly_payment = '';
	public $schelude_duration = '';
	public $id_code = '';
	public $surname = '';
	public $firstname = '';
	public $phone = '';
	public $email = '';
	public $address = '';
	public $employer = '';
	public $time_with_employer = '';
	public $salary = '';

	public static $definition = array(
		'table' => 'liisi',
		'primary' => 'id_liisi',
		'multilang' => false,
		'fields' => array(
			'id_order' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'contract_number' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'contract_fee' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'down_payment' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'postal_fee' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'monthly_payment' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'schelude_duration' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'id_code' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'surname' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'firstname' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'phone' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'email' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'address' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'employer' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'time_with_employer' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'salary' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
		)
	);


	/**
	* Get Liisi record by Order ID
	* @param $_id_order
	*
	* @return LiisiModel
	**/
	public static function getByOrderId($id_order)
	{
		if (!$id_order) return false;
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'liisi` WHERE `id_order`='.(int)$id_order;
		$result = Db::getInstance()->executeS($sql);
		if ($result)
		{
			$liisi = new LiisiModel();
			foreach ($result[0] as $key => $val)
			{
				if ($key == 'id_liisi')
				{
					$liisi->id = $val;
				}
				else
				{
					$liisi->{$key} = $val;
				}
			}
			return $liisi;
		}
		return false;
	}

}
